/*
* ClearURLs
* Copyright (c) 2017-2020 Kevin Röbert
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*jshint esversion: 6 */

/**
* Load only when document is ready
*/
(function() {
    setText();
    loadWhitelist();
    document.getElementById('whitelists_btn').onclick = saveWhitelist;
})();

/**
* This function saves all URLs line by line in the textarea for whitelisting.
*/
function saveWhitelist() {
    const urlsBox = document.getElementById('whitelistedURLs');
    const urls = urlsBox.value.split('\n');

    let temp = [];

    for (let i = 0; i < urls.length; i++) {
        temp.push(urls[i]);
    }

    browser.runtime.sendMessage({
        function: "setData",
        params: ['WhitelistedURLs', JSON.stringify(temp)]
    }).catch(handleError);

    loadWhitelist();
}

/**
 * This function loads all URLs line by line in the textarea for whitelisting.
 */
function loadWhitelist() {
    const urlsBox = document.getElementById('whitelistedURLs');
    urlsBox.value = "";

    browser.runtime.sendMessage({
        function: "getData",
        params: ['WhitelistedURLs']
    }).then((data) => {
        let arr = data.response;

        for (let i = 0; i < arr.length; i++) {
            urlsBox.value = urlsBox.value + arr[i];

            if(i < arr.length - 1) {
                urlsBox.value = urlsBox.value + "\n";
            }
        }
    }).catch(handleError);
}

/**
 * Translate a string with the i18n API.
 *
 * @param {string} string Name of the attribute used for localization
 */
function translate(string)
{
    return browser.i18n.getMessage(string);
}

/**
* Set the text for the UI.
*/
function setText()
{
    document.title = translate('whitelists_page_title');
    document.getElementById('page_title').textContent = translate('whitelists_page_title');
    document.getElementById('whitelists_description').textContent = translate('whitelists_description');
    document.getElementById('whitelists_btn').textContent = translate('whitelists_btn');
    document.getElementById('whitelists_urls_label').textContent = translate('whitelists_urls_label');
}

function handleError(error) {
    console.log(`Error: ${error}`);
}